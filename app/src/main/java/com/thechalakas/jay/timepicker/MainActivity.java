package com.thechalakas.jay.timepicker;
/*
 * Created by jay on 15/09/17. 8:46 PM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.Date;

public class MainActivity extends AppCompatActivity
{


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

        timePicker.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //timePicker.toString();
                Log.i("MainActivity",timePicker.toString());
            }

        });

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener()
        {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1)
            {
                Integer hour = timePicker.getCurrentHour();
                Integer minutes = timePicker.getCurrentMinute();

                String time_message = "Hour is "+hour.toString()+" Minute is "+ minutes.toString();
                Log.i("onTimeChanged",time_message);
            }
        });

    }
}
